variable "aws_region" {
default = "ap-south-1"
}

variable "jenkins_instance_type" {
default = "t2.micro"
}

variable "jenkins_ami" {
default = "ami-01a4f99c4ac11b03c"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "levelup_key"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "levelup_key.pub"
}

variable "INSTANCE_USERNAME" {
  default = "ec2-user"
}